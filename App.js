import React, { useState } from 'react'
import { Button, Text, View, StyleSheet, KeyboardAvoidingView,Platform } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { PromoStackScreen } from "./src/pages/Promo";
import { DiscountCardStackScreen } from "./src/pages/Discount";
import { ProfileStackScreen } from "./src/pages/Profile";
import { StoresStackScreen } from "./src/pages/Stores";
import { Tab, CardStack, Icon} from "./src/Global";
import { useFonts } from "expo-font";
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';
import { createIconSetFromIcoMoon } from "@expo/vector-icons";
import AppLoading from "expo-app-loading";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

export default function App() {
  
  const [fontsLoaded] = useFonts({
    IcoMoon: require("./src/font/icomoon/fonts/icomoon.ttf"),
  });
  if (!fontsLoaded) {
    return <AppLoading />;
  }

  return (
    <SafeAreaProvider>
    <NavigationContainer>
      
      <Tab.Navigator style={styles.container}
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;
            if (route.name === "Discount") {
              iconName = "card";
            } else if (route.name === "Promo") {
              iconName = "promo";
            } else if (route.name === "Stores") {
              iconName = "shop";
            } else if (route.name === "Profile") {
              iconName = "user";
            }

            return <Icon name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: "orange",
          inactiveTintColor: "gray",
        }}
      >
        <Tab.Screen
          options={{ title: "Акции" }}
          name="Promo"
          component={PromoStackScreen}
        />
        <Tab.Screen
          options={{
            title: "Магазины",
          }}
          name="Stores"
          component={StoresStackScreen}
        />
        <Tab.Screen
          options={{
            title: "Мой дисконт",
          }}
          name="Discount"
          component={DiscountCardStackScreen}
          initialParams={{ itemId: 42 }}
        />
        <Tab.Screen
          options={{
            title: "Профиль",
          }}
          name="Profile"
          component={ProfileStackScreen}
        />
      </Tab.Navigator>
      
    </NavigationContainer>
    </SafeAreaProvider>
    
  );
}
const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
    paddingVertical: 20
  }
})