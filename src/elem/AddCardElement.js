import React, { useState, useEffect, useRef } from "react";
import { Text, View, TextInput, ScrollView, StyleSheet } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { and } from "react-native-reanimated";

export const AddCardElem = ({ onSubmit }) => {

  const [value, setValue] = useState("");

  const pressHandler = () => {
    if (value.trim().length===16  ) {
      
      onSubmit(value);
      setValue("");
    }
  };
  const checkDigit = (text) => {
    setValue(text.replace(/[^0-9]/g, ''));
  }
  
  return (
    <View style={styles.block}>
      <ScrollView keyboardShouldPersistTaps="always">


        <TextInput
          placeholder="Номер карты"
          style={styles.textIn}
          onChangeText={checkDigit}
          value={value}
          autoFocus={true}
          autoCorrect={false}
          autoCapitalize="none"
          keyboardType="number-pad"
          textContentType="creditCardNumber"
          maxLength={16}
        />
      </ScrollView>
      <TouchableOpacity style={styles.cardAddButton} onPress={pressHandler}>
        <Text style={styles.buttonText}>Зарегистрировать карту</Text>
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  cardAddButton: {
    width: "90%",
    paddingVertical: 10,
    backgroundColor: "#007AFF",
    borderRadius: 20,
    marginLeft:"5%"
  },
  buttonText: {
    color: "#fff",
    textAlign: "center",
  },
  textIn:{
    fontSize:30,
    borderRadius:10,
    borderWidth:2,
    textAlign:"left",
    borderColor:"#808080",
    marginBottom: "5%",
    paddingLeft:10
  },
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: "#fff",
  },
  scanScreenMessage: {
    fontSize: 20,
    color: "white",
    textAlign: "center",
    alignItems: "center",
    justifyContent: "center",
  },
});
