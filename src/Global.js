import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createIconSetFromIcoMoon } from "@expo/vector-icons";

export { CardStack, Tab };

export const Icon = createIconSetFromIcoMoon(
  require("./font/icomoon/selection.json"),
  "IcoMoon",
  "icomoon.ttf"
);

const CardStack = createStackNavigator();
const Tab = createBottomTabNavigator();
