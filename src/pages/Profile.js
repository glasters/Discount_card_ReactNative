import * as React from "react";
import { Button, Text, View, Image } from "react-native";
import { CardStack } from "../Global";
export { ProfileStackScreen };
function ProfileScreen({ navigation }) {
  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Text>Profile screen</Text>
    </View>
  );
}

function ProfileStackScreen() {
  return (
    <CardStack.Navigator screenOptions={{ headerTitleAlign: "center" }}>
      <CardStack.Screen
        options={{
          headerTitle: (
            props // App Logo
          ) => (
            <Image
              style={{ width: 200, height: 50 }}
              source={require("../img/icon.png")}
              resizeMode="contain"
            />
          ),
          headerTitleStyle: { flex: 1, textAlign: "center" },
        }}
        name="Profile"
        component={ProfileScreen}
      />
    </CardStack.Navigator>
  );
}
