import React, { useState, useEffect, useRef } from "react";
import {
  Button,
  Text,
  View,
  Image,
  StyleSheet,
  TextInput,
  Alert,
  FlatList,
  KeyboardAvoidingView,
  Keyboard,
  ScrollView,
} from "react-native";
import { TouchableOpacity as TouchableOpacity2 } from "react-native";
import Modal from "react-native-modal";
import { BarCodeScanner } from "expo-barcode-scanner";
import { Dialog, ConfirmDialog } from "react-native-simple-dialogs";
import { TouchableOpacity } from "react-native-gesture-handler";
import Barcode from "react-native-barcode-expo";
import { CardStack, Icon } from "../Global";
import BottomSheet from "reanimated-bottom-sheet";
import { AddCardElem } from "../elem/AddCardElement";
export { DiscountCardStackScreen, DiscountCardScreen };

//Render one card
export const CardItem = ({ card, navigation, sheetRefInfo, onRemove }) => {
  return (
    <View
      style={{
        alignItems: "center",
        flex: 1,
      }}
    >
      <View
        style={{
          minWidth: "100%",
          alignItems: "center",
          borderBottomLeftRadius: 40,
          borderBottomRightRadius: 40,
          backgroundColor: "orange",
        }}
      >
        <Text
          style={{
            marginTop: "5%",
            fontSize: 20,
            marginTop: 20,
            color: "white",
            fontWeight: "bold",
          }}
        >
          СКИДКА ЛЮБИМОГО КЛИЕНТА
        </Text>
        <Text style={{ fontSize: 40, marginBottom: 20, color: "white" }}>
          {card.discount == undefined ? "" : card.discount + "%"}
        </Text>
      </View>
      <Text
        style={{
          marginTop: "5%",
          marginBottom: "5%",
          fontSize: 16,
          color: "black",
          fontWeight: "bold",
        }}
      >
        Штрих код на карте
      </Text>
      <Barcode value={card.id} format="CODE128" />

      <Text
        style={{
          marginTop: "5%",
          fontSize: 16,
          color: "black",
          fontWeight: "bold",
        }}
      >
        Номер карты
      </Text>
      <Text style={{ marginTop: "5%", fontSize: 32 }}>
        {card.id.substr(0, 4) +
          " " +
          card.id.substr(4, 4) +
          " " +
          card.id.substr(8, 4) +
          " " +
          card.id.substr(12, 4)}
      </Text>
      <TouchableOpacity
        style={{ marginTop: "35%" }}
        onPress={() => {
          navigation.dangerouslyGetParent().setOptions({
            tabBarVisible: false,
          });
          sheetRefInfo.current.snapTo(0);
        }}
      >
        <Text style={{ color: "blue" }}>Информация о возможностях карты</Text>
      </TouchableOpacity>
      <TouchableOpacity style={{ marginTop: "5%" }} onPress={onRemove}>
        <Text style={{ color: "gray" }}>Удалить карту</Text>
      </TouchableOpacity>
    </View>
  );
};
function renderHeader() {
  return (
    <View style={styles.header}>
      <View style={styles.panelHeader}>
        <View style={styles.panelHandle} />
      </View>
    </View>
  );
}
function LogoTitle() {
  return (
    <Image
      style={{ width: 200 }}
      source={require("../img/icon.png")}
      resizeMode="contain"
    />
  );
}
//Discount stack
function DiscountCardStackScreen() {
  return (
    <CardStack.Navigator screenOptions={{ headerTitleAlign: "center" }}>
      <CardStack.Screen
        options={{
          headerTitle: (props) => <LogoTitle {...props} />,
          headerTitleStyle: { flex: 1, textAlign: "center" },
        }}
        name="Discount"
        component={DiscountCardScreen}
      />

      <CardStack.Screen
        name="RegCardQR"
        component={RegCardScreenQR}
        options={{
          headerTitle: (props) => <LogoTitle {...props} />,
          headerTitleStyle: { flex: 1, textAlign: "center" },
        }}
      />
    </CardStack.Navigator>
  );
}
//Page Discount
function DiscountCardScreen({ navigation }) {
  const [cardInfo, setInfoCard] = useState([]);
  const [cardInfos, setInfosCard] = useState([
    {
      id: "1111111111111111",
      discount: 5,
      info: `Ваша дисконтная карта дает скидку -5% на покупку всех товаров возможность получения скидки в торговых точках магазина или участников дисконтного клуба при соблюдении правил использования дисконтных карт. 
      
      Скидка не суммируется со скидкой акционных товаров.`,
    },
    {
      id: "2222222222222222",
      discount: 10,
      info: `Ваша дисконтная карта дает скидку -10% на покупку всех товаров возможность получения скидки в торговых точках магазина или участников дисконтного клуба при соблюдении правил использования дисконтных карт. 
      
      Cкидка не суммируется со скидкой акционных товаров.`,
    },
    {
      id: "4222222223242344",
      discount: 10,
      info: `Ваша дисконтная карта дает скидку -10% на покупку всех товаров возможность получения скидки в торговых точках магазина или участников дисконтного клуба при соблюдении правил использования дисконтных карт. 
      
      Cкидка не суммируется со скидкой акционных товаров.`,
    },
    {
      id: "2021202120121212",
      discount: 10,
      info: `Ваша дисконтная карта дает скидку -10% на покупку всех товаров возможность получения скидки в торговых точках магазина или участников дисконтного клуба при соблюдении правил использования дисконтных карт. 
      
      Cкидка не суммируется со скидкой акционных товаров.`,
    },
  ]);

  const [modalRegOk, setRegOk] = useState(false);
  const [modalDelete, setModalDelete] = useState(false);
  const AddCard = (id) => {
    if (id.trim().length === 16) {
      sheetRefReg.current.snapTo(2);
      let cur = cardInfos.filter((cardInfo) => cardInfo.id == id);
      if (Object.keys(cur).length !== 0 && id >= 0) {
        navigation.setOptions({
          headerStyle: {
            backgroundColor: "orange",
            borderBottomWidth: 0,
            elevation: 0,
            shadowOpacity: 0,
            borderBottomWidth: 0,
          },
        });
        setRegOk(true);
        setInfoCard(cur);
      } else {
      }
    }
  };

  const removeCard = () => {
    navigation.setOptions({ headerStyle: {} });
    setInfoCard([]);
  };
  const modalRemoveCard = () => {
    setModalDelete(true);
  };
  const removeCardClose = () => {
    setModalDelete(false);
    removeCard();
  };
  const renderContentRegister = () => (
    <View
      style={{
        backgroundColor: "white",
        padding: 16,
        height: "100%",
      }}
    >
      <Text style={styles.textBold}>Регистрация карты</Text>
      <Text style={styles.textGray}>Введите номер карты для регистрации</Text>
      <AddCardElem onSubmit={AddCard} setRegOk={setRegOk} />
      <TouchableOpacity
        style={styles.cardAddButtonS}
        onPress={() =>
          navigation.navigate("RegCardQR", {
            AddCard: AddCard,
            setRegOk: setRegOk,
          })
        }
      >
        <Text style={styles.buttonText}>Отсканировать карту </Text>
      </TouchableOpacity>
    </View>
  );
  const sheetRefReg = React.useRef(null);
  const sheetRefInfo = React.useRef(null);
  return (
    <View style={{ flex: 1, minHeight: "100%", alignItems: "center" }}>
      {Object.keys(cardInfo).length !== 0 ? null : (
        <Icon
          style={{ marginTop: "50%" }}
          name="card"
          size={75}
          color="lightgray"
        />
      )}
      {Object.keys(cardInfo).length !== 0 ? null : (
        <TouchableOpacity
          style={styles.cardAddButton}
          onPress={() => {
            sheetRefReg.current.snapTo(0);
            navigation.dangerouslyGetParent().setOptions({
              tabBarVisible: false,
            });
          }}
        >
          <Text style={styles.buttonText}>Зарегистрировать карту</Text>
        </TouchableOpacity>
      )}

      <BottomSheet
        ref={sheetRefReg}
        initialSnap={2}
        snapPoints={["90%", "80%", 0]}
        borderRadius={10}
        renderContent={renderContentRegister}
        renderHeader={renderHeader}
        onCloseEnd={() => {
          Keyboard.dismiss();
          navigation.dangerouslyGetParent().setOptions({
            tabBarVisible: true,
          });
        }}
      />

      <Dialog
        visible={modalRegOk}
        onTouchOutside={() => setRegOk(false)}
        dialogStyle={{ borderRadius: 20 }}
      >
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            minHeight: "170%",
            paddingBottom: "90%",
            textAlign: "center",
          }}
        >
          <Icon
            style={{ marginLeft: "45%", marginBottom: "5%" }}
            name="hand-stop"
            size={40}
            color="#F6821C"
          ></Icon>
          <Text
            style={{
              textAlign: "center",
              color: "#F6821C",
              fontWeight: "bold",
              fontSize: 22,
            }}
          >
            Карта{"\n"}зарегистрирована!
          </Text>
          <Text style={{ textAlign: "center", fontSize: 16, marginTop: "5%" }}>
            Спасибо за регистрацию карты.{"\n"}информация о возможностях карты
            находится на этой вкладке
          </Text>
          <TouchableOpacity2
            style={styles.cardAddButtonS}
            onPress={() => setRegOk(false)}
          >
            <Text style={styles.buttonText}>Понятно </Text>
          </TouchableOpacity2>
        </View>
      </Dialog>
      <Dialog
        dialogStyle={{ borderRadius: 20 }}
        visible={modalDelete}
        onTouchOutside={() => setModalDelete(false)}
      >
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            minHeight: "150%",
            paddingBottom: "50%",
            textAlign: "center",
          }}
        >
          <Icon
            style={{ marginLeft: "45%", marginBottom: "5%" }}
            name="bin"
            size={40}
            color="#F6821C"
          ></Icon>
          <Text
            style={{
              textAlign: "center",
              color: "#F6821C",
              fontWeight: "bold",
              fontSize: 16,
            }}
          >
            Вы действительно{"\n"}хотите удалить карту?
          </Text>
          <View style={{ flexDirection: "row" }}>
            <TouchableOpacity2
              style={{
                width: "40%",
                paddingVertical: 10,
                backgroundColor: "transparent",
                borderRadius: 20,
                marginLeft: "5%",
                marginTop: "5%",
              }}
              onPress={() => setModalDelete(false)}
            >
              <Text style={{ textAlign: "center", color: "blue" }}>
                Отменить{" "}
              </Text>
            </TouchableOpacity2>
            <TouchableOpacity2
              style={{
                width: "40%",
                paddingVertical: 10,
                backgroundColor: "#007AFF",
                borderRadius: 20,
                marginLeft: "5%",
                marginTop: "5%",
              }}
              onPress={() => removeCardClose()}
            >
              <Text style={styles.buttonText}>Удалить </Text>
            </TouchableOpacity2>
          </View>
        </View>
      </Dialog>

      {Object.keys(cardInfo).length == 0 ? null : (
        <FlatList
          keyExtractor={(item) => item.id.toString()}
          data={cardInfo}
          renderItem={({ item }) => (
            <CardItem
              card={item}
              navigation={navigation}
              sheetRefInfo={sheetRefInfo}
              onRemove={modalRemoveCard}
            />
          )}
        />
      )}
      <BottomSheet
        ref={sheetRefInfo}
        initialSnap={2}
        snapPoints={["55%", "55%", 0]}
        borderRadius={10}
        renderContent={() => (
          <View
            style={{
              backgroundColor: "white",
              padding: 16,
              height: "100%",
            }}
          >
            <Text style={styles.textBold}>Возможности карты</Text>
            <Text style={{ marginTop: "5%" }}>
              {cardInfo?.[0]?.info == undefined ? "" : cardInfo?.[0]?.info}
            </Text>
          </View>
        )}
        renderHeader={renderHeader}
        onCloseEnd={() => {
          Keyboard.dismiss();
          navigation.dangerouslyGetParent().setOptions({
            tabBarVisible: true,
          });
        }}
      />
    </View>
  );
}
export function RegCardScreenQR({ route, navigation }) {
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === "granted");
    })();
  }, []);

  const handleBarCodeScanned = ({ type, data }) => {
    setScanned(true);
    route.params.AddCard(data);
    route.params.setRegOk(true);
    navigation.navigate("Discount");
  };

  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  return (
    <View style={styles.container}>
      <View style={{ flex: 1 }}>
        <BarCodeScanner
          onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
          style={StyleSheet.absoluteFillObject}
        />
      </View>
    </View>
  );
}
////// Styles
const styles = StyleSheet.create({
  cardAddButton: {
    minWidth: "80%",
    marginTop: "10%",
    paddingVertical: 10,
    backgroundColor: "#007AFF",
    borderRadius: 20,
  },
  cardAddButtonS: {
    width: "90%",
    paddingVertical: 10,
    backgroundColor: "#007AFF",
    borderRadius: 20,
    marginLeft: "5%",
    marginTop: "5%",
  },
  buttonText: {
    color: "#fff",
    textAlign: "center",
  },
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: "#fff",
  },
  scanScreenMessage: {
    fontSize: 20,
    color: "white",
    textAlign: "center",
    alignItems: "center",
    justifyContent: "center",
  },
  header: {
    backgroundColor: "transparent",
    shadowColor: "#000000",
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  panelHeader: {
    alignItems: "center",
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: "#00000040",
    marginBottom: 10,
  },
  textBold: {
    fontWeight: "bold",
    fontSize: 22,
  },
  textGray: {
    marginTop: "5%",
    marginBottom: "5%",
    color: "#808080",
    fontSize: 12,
  },
});
