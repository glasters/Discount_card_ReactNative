import * as React from "react";
import { Button, Text, View, Image } from "react-native";
import { CardStack } from "../Global";
export { StoresStackScreen };
function StoresScreen({ navigation }) {
  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Text>Stores screen</Text>
    </View>
  );
}

function StoresStackScreen() {
  return (
    <CardStack.Navigator screenOptions={{ headerTitleAlign: "center" }}>
      <CardStack.Screen
        options={{
          headerTitle: (
            props // App Logo
          ) => (
            <Image
              style={{ width: 200, height: 50 }}
              source={require("../img/icon.png")}
              resizeMode="contain"
            />
          ),
          headerTitleStyle: {
            flex: 1,
            textAlign: "center",
            alignItems: "center",
            justifyContent: "center",
          },
          headerLayoutPreset: "center",
        }}
        name="Stores"
        component={StoresScreen}
      />
    </CardStack.Navigator>
  );
}
